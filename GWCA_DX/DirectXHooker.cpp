#include "DirectXHooker.h"

void* GW::DirectXHooker::detail::dx_backup_vtable[GW::dx9::kEnd];
IDirect3DDevice9* GW::DirectXHooker::detail::dx_device = nullptr;

namespace {
	void** dx_vtable = nullptr;
}

IDirect3DDevice9* GW::DirectXHooker::Initialize() {
	using namespace detail;
	struct d3ddev_ptr_chain {
		struct sub2 {
			BYTE pad[0x90];
			IDirect3DDevice9* device;
		}**dx_array; // Index
		DWORD sub2_size_alloc;
		DWORD sub2_size;
	} *base = nullptr;

	const BYTE dxdevicebasecode[] = { 0x89,0x45,0xF4,0x8B,0x45,0xFC,0x33,0xF6 };

	for (BYTE* i = (BYTE*)0x401000; i < (BYTE*)0x900000; ++i)
	{
		if (!memcmp(i, dxdevicebasecode, sizeof(dxdevicebasecode)))
		{
			base = *(d3ddev_ptr_chain**)(i - 0xE);
			break;
		}
	}

	dx_device = base->dx_array[1]->device;

	dx_vtable = *(void***)dx_device;

	memcpy(dx_backup_vtable, dx_vtable, dx9::kEnd * sizeof(void*));

	return dx_device;
}

void GW::DirectXHooker::RemoveAllHooks() {
	for (DWORD index = 0; index < dx9::kEnd; ++index) {
		if (dx_vtable[index] != detail::dx_backup_vtable[index]) {
			RemoveHook((dx9::e_DX9VtableIndex)index);
		}
	}
}

void GW::DirectXHooker::AddHook(dx9::e_DX9VtableIndex index, void* function) {
	if (dx_vtable == nullptr) return;
	DWORD oldprot;
	VirtualProtect(&dx_vtable[index], sizeof(void*), PAGE_READWRITE, &oldprot);
	dx_vtable[index] = function;
	VirtualProtect(&dx_vtable[index], sizeof(void*), oldprot, &oldprot);
}

void GW::DirectXHooker::RemoveHook(dx9::e_DX9VtableIndex index) {
	if (dx_vtable == nullptr) return;
	DWORD oldprot;
	VirtualProtect(&dx_vtable[index], sizeof(void*), PAGE_READWRITE, &oldprot);
	dx_vtable[index] = detail::dx_backup_vtable[index];
	VirtualProtect(&dx_vtable[index], sizeof(void*), oldprot, &oldprot);
}
