#pragma once

#include "DirextXDeclarations.h"

namespace GW {

	namespace DirectXHooker {
		namespace detail {
			extern void* dx_backup_vtable[GW::dx9::kEnd];
			extern IDirect3DDevice9* dx_device;
		}

		IDirect3DDevice9* Initialize();

		inline IDirect3DDevice9* GetDevice() {
			return detail::dx_device;
		}

		template <typename T>
		inline T Original(dx9::e_DX9VtableIndex index) {
			return (T)detail::dx_backup_vtable[index];
		}

		void AddHook(dx9::e_DX9VtableIndex index, void* function);

		void RemoveHook(dx9::e_DX9VtableIndex index);

		void RemoveAllHooks();
	};
}
